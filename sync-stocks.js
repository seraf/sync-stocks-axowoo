const config = require('./secrets.config.json');
const axios = require('axios').default;
const WooCommerceRestApi = require('@woocommerce/woocommerce-rest-api').default;
const csvToJson = require('csv-file-to-json');
const CronJob = require('cron').CronJob;
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const wooCommerceApi = new WooCommerceRestApi(config.wooCommerce);

// example : 31/12/2020
const dateFormatterForAxonaut = new Intl.DateTimeFormat('fr-FR', { timeZone: 'Europe/Paris' });

const logger = createLogger({
    format: combine(
        timestamp({ format: 'YYYY/MM/DD HH:mm:ss' }),
        printf(({ level, message, timestamp }) => `${timestamp} [${level}] : ${JSON.stringify(message)}`)
    ),
    transports: [
        new transports.File({ filename: 'error.log', level: 'error' }),
        new transports.File({ filename: 'combined.log' })
    ]
});

const handleAxiosError = (error) => {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        logger.error({ statusCode: error.response.status });
        logger.error({ headers: error.response.headers });
        logger.error({ data: error.response.data });
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        logger.error({ request: error.request });
    } else {
        // Something happened in setting up the request that triggered an Error
        logger.error({ message: error.message });
    }
    logger.error({ config: error.config });
}

const axonautGET = async (url, params) => {
    const headers = { Accept: 'application/json', userApiKey: config.axonaut.userApiKey };
    const response = await axios.get(url, { headers, params });
    return response ? response.data : null;
}

const getInvoicesFromAxonautSince = (dateAfter) => {
    const url = `${config.axonaut.baseUrl}${config.axonaut.endpoints.invoices}`;
    const date_after = dateFormatterForAxonaut.format(new Date(dateAfter));
    logger.debug({ date_after });
    const params = { sort: 'updatetime', date_after };
    return axonautGET(url, params);
}

const getCurrentAxonautStock = (productId) => {
    const url = `${config.axonaut.baseUrl}${config.axonaut.endpoints.products}/${productId}/stock`;
    return axonautGET(url);
}

const updateWooCommerceStock = async (stock, productId, variationId) => {
    try {
        logger.debug({ stock, productId, variationId });
        const productUrl = `products/${productId}`;
        const result = null;
        if (!variationId) {
            await wooCommerceApi.put(productUrl, { stock_quantity: stock });
        } else {
            await wooCommerceApi.put(productUrl + `/variations/${variationId}`, { stock_quantity: stock });
        }
    } catch (error) {
        handleAxiosError(error);
    }
}

const filterAndBuildListOfProductsToBeUpdated = (axonautInvoices) => {
    const productsFromCsvInJson = csvToJson({ filePath: './secret.lbdb.csv' });
    return axonautInvoices
        .filter((invoice) => invoice.order_number < 1000 || invoice.order_number === null)
        .map(invoice => {
            const linesLength = invoice.invoice_lines.length;
            return invoice.invoice_lines.map((line, index) => {
                const { product_code, product_name } = line;
                const products = productsFromCsvInJson.filter((product) => product.REFERENCE === product_code);
                if (products.length === 1) {
                    return {
                        invoiceNumber: invoice.number,
                        invoiceName: product_name,
                        line: (index + 1) + '/' + linesLength,
                        product: { ...products[0] }
                    };
                } else {
                    logger.warn(`Invalid : le produit avec la réference=${product_code} a été trouvé ${products.length} fois dans le fichier CSV.`)
                }
            })
        })
        .flat()
        .filter(Boolean);
}

const synchronizeStocks = async (itemsToBeUpdated) => {
    return await Promise.all(
        itemsToBeUpdated.map(async (item) => {
            const { current_stock } = await getCurrentAxonautStock(item.product.AXONAUT_ID);
            await updateWooCommerceStock(current_stock, item.product.WOO_ID, item.product.WOO_VARIATION_ID);
            logger.debug({ "[UPDATE_WOO] DONE": { product: item.product, current_stock } });
            return { ...item, current_stock };
        })
    );
}

const synchronizeWooStocksWithAxonautStocks = async () => {
    try {
        const axonautInvoices = await getInvoicesFromAxonautSince(new Date());
        logger.debug({ invoicesCount: axonautInvoices.length });
        const productsToBeUpdated = filterAndBuildListOfProductsToBeUpdated(axonautInvoices);
        logger.debug({ productsToBeUpdatedCount: productsToBeUpdated.length });
        if (productsToBeUpdated.length) {
            const listOfItemsUpdated = await synchronizeStocks(productsToBeUpdated);
            logger.info(`${listOfItemsUpdated.length} PRODUCTS UPDATED`);
            logger.info({ listOfItemsUpdated });
        } else {
            logger.info('Nothing to update.');
        }
    } catch (error) {
        handleAxiosError(error);
    }
};

if (config.app.production) {
    const cronJob = new CronJob(
        config.app.cronTime.from11to19MondayTroughFriday,
        synchronizeWooStocksWithAxonautStocks,
        null,
        true,
        'Europe/Paris'
    );
    cronJob.start();
} else {
    logger.add(new transports.Console({ level: 'debug' }));
    synchronizeWooStocksWithAxonautStocks();
}
