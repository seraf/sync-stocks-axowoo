## Purpose

- update item’s stocks in woocommerce account according to item’s stocks in axonaut account
- use cron to launch the program every hour

Data in axonaut account are the reference, and we want to update woocommerce stocks according to axonaut stocks.

## Requirements

### technical

- an installed nodejs server

### manual

- have a CSV file in which there is a matching between IDs from axonaut account & woocommerce account

#### Scenario

1. extract in a csv file ids from axonaut & ids from woocommerce (an excel macro could be used)
2. name the file "secret.lbdb.csv"
3. launch the application (see below)

## Getting started

1. make a copy of template.config.json

```javascript
cp template.config.json secrets.config.json
```

2. fill secrets.config.json with your secrets

3. install dependencies
```javascript
npm install
```

4. run the script
```javascript
node sync-stocks.js
```

## Go further

You will want to start the program as a daemon (detached mode), using some tools like [PM2](https://pm2.keymetrics.io/) or [forever](https://www.npmjs.com/package/forever).

For example, [see here for help](https://stackoverflow.com/questions/4018154/how-do-i-run-a-node-js-app-as-a-background-service).